﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
namespace DiceGame
{
    [Activity(Label = "DiceGame", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {

        ImageView img;
        Button b;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            b = FindViewById<Button>(Resource.Id.button1);
            img = FindViewById<ImageView>(Resource.Id.imageView1);

            b.Click += RollDice;
        }

        public void RollDice(object o, EventArgs e) {
            Random r = new Random();
            int rollResult = r.Next(1,7);   // generate a random number between 1 and 6

            Console.WriteLine(rollResult);


            if (rollResult == 1) {
                img.SetImageResource(Resource.Drawable.dice1);
            }
            else if (rollResult == 2) {
                img.SetImageResource(Resource.Drawable.dice2);
            }
            else if (rollResult == 3) {
                img.SetImageResource(Resource.Drawable.dice3);
            }
            else if (rollResult == 4) {
                img.SetImageResource(Resource.Drawable.dice4);
            }
            else if (rollResult == 5) {
                img.SetImageResource(Resource.Drawable.dice5);
            }
            else if (rollResult == 6) {
                img.SetImageResource(Resource.Drawable.dice1);
            }

        }
    }
}

